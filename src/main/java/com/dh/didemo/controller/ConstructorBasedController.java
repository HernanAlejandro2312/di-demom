package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingService;


public class ConstructorBasedController {
    private GreetingService greetingService;

    public ConstructorBasedController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello() {
        return greetingService.sayGreeting();
    }
}
