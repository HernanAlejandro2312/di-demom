package com.dh.didemo.controller;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    public String Hello() {
        String greeting = "Hello Spring";
        System.out.println(greeting);
        return greeting;

    }
}
