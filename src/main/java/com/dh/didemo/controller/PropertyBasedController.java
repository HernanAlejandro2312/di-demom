package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingService;

public class PropertyBasedController {
    public GreetingService greetingService;

    public String sayHello() {
        return greetingService.sayGreeting();
    }
}
