package com.dh.didemo.services;

public class GreetingServiceImpl implements GreetingService {
    public static final String GREETING = "Hello GreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
