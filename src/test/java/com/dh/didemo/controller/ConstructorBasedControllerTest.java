package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingService;
import com.dh.didemo.services.GreetingServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConstructorBasedControllerTest {
    private ConstructorBasedController constructorBasedController;

    @Before
    public void before() throws Exception {
        System.out.println("@Before");
        GreetingService greetingService = new GreetingServiceImpl();
        constructorBasedController = new ConstructorBasedController(greetingService);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");

    }

    @Test
    public void sayHello() {
        System.out.println("@Test say Hello");
        String greeting = constructorBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING, greeting);

    }
}